import tkinter as tk
from tkinter import filedialog 
import pyperclip

opened_file = None

# Function for opening the  
# file explorer window 
def browseFiles(): 
    filename = filedialog.askopenfilename(initialdir = "/", 
                                          title = "Select a File", 
                                          filetypes = (("Text files", 
                                                        "*.txt*"), 
                                                       ("all files", 
                                                        "*.*"))) 
    
    try:
        global opened_file
        opened_file = open(filename,'a')
    except:
        print("No file exists")
    # Change label contents 
    label_file_explorer.configure(text="File Opened: "+filename) 


def name_to_folder_name():
    text="BitYadly.ir - "
    text+=ent_name.get()

    copy_to_clipboard(text)
    lbl_result_1_msg["text"] = "copied"
    lbl_result_1_msg["fg"] = "green"

    lbl_result_2_msg["text"] = "..."
    lbl_url_msg["text"] = "..."
    add_to_file_msg["text"] = "..."



def name_to_Rar_file_name():
    text="BitYadly.ir - "
    text+=ent_name.get()

    for i in range(len(text)):
        if text[i-1]==" ":
            temp_x=text[i].capitalize()
            text=text[:i]+temp_x+text[i+1:]

    text= text.replace(" ","")
    text+=".rar"

    copy_to_clipboard(text)
    lbl_result_2_msg["text"] = "copied"
    lbl_result_2_msg["fg"] = "green"


def name_to_Rar_file_name_for_url():
    text="BitYadly.ir - "
    text+=ent_name.get()

    for i in range(len(text)):
        if text[i-1]==" ":
            temp_x=text[i].capitalize()
            text=text[:i]+temp_x+text[i+1:]

    text= text.replace(" ","")
    return text

def url_maker():
    temp_url="http://"
    temp_url+=ent_url.get()
    temp_url+="/"
    temp_url+=name_to_Rar_file_name_for_url()
    temp_url+=".part"

    return temp_url

def full_url_maker():
    if int(ent_url_count.get()) > 0 :
        temp_copy=""
        for i in range(int(ent_url_count.get())):
            temp_url=url_maker()

            if int(ent_url_count.get()) == 1:
                temp_url=temp_url[:-5]
            elif int(ent_url_count.get()) >= 10 and i < 9:
                temp_url+="0"

            if int(ent_url_count.get())>1:
                temp_url+=str(i+1)
            temp_url+=".rar"

            temp_copy+=temp_url
            temp_copy+="\n"

        lbl_url_msg["text"] = "copied"
        lbl_url_msg["fg"] = "green"
        
        copy_to_clipboard(temp_copy)
    else:
        lbl_url_msg["text"] = "Enter a valid Number of Parts"
        lbl_url_msg["fg"] = "red"


def copy_to_clipboard(x):
    pyperclip.copy(x)



def add_to_end_of_the_file():
    global opened_file
    temp_url_num = 0

    try: 
        temp_url_num = int(ent_url_count.get())
    except ValueError:
        add_to_file_msg["text"] = "Enter a (Number) please!"
        add_to_file_msg["fg"] = "red"

    if temp_url_num < 1:
        add_to_file_msg["text"] = "Enter a valid Number of Parts"
        add_to_file_msg["fg"] = "red"
    elif opened_file:
        full_url_maker()
        temp_val = pyperclip.paste()
        opened_file.write('\n')
        opened_file.write('-'*100)
        opened_file.writelines(temp_val)
        opened_file.flush()
        # opened_file.close()
        add_to_file_msg["text"] = "Added to End of the Opened File!"
        add_to_file_msg["fg"] = "green"
    else:
        add_to_file_msg["text"] = "There is No Opened File!"
        add_to_file_msg["fg"] = "red"


window = tk.Tk()
window.title("BitYadly.ir Renamer")
window.columnconfigure(0, minsize=250)
window.rowconfigure(0, minsize=100)
window.columnconfigure([1, 2], minsize=200)
window.rowconfigure([1, 2, 3, 4, 5, 6, 7], minsize=30)

frame_a = tk.Frame(master=window)

label = tk.Label(
    master=frame_a,
    text="""
    تبدیل اسم به نام بیت یاد
    ساخته شده توسط آقا جـــــــواد
    """,
    font=("FarhangFaNum MediumSharp", 20)
)

label.grid(row=0, column=0)
frame_a.grid(row=0, column=0, columnspan = 3)


frm_entry_1 = tk.Frame(master=window)
frm_entry_2 = tk.Frame(master=window)

frm_entry_1.columnconfigure([0, 1], minsize=200)
frm_entry_1.rowconfigure([0, 1, 2], minsize=25)

ent_name = tk.Entry(master=frm_entry_1, width=55)
lbl_name = tk.Label(master=frm_entry_1, text="Enter the Name: ")

ent_url = tk.Entry(master=frm_entry_1, width=30)
lbl_url = tk.Label(master=frm_entry_1, text="Enter the server ip/Domain: ")
ent_url.insert(tk.END, '51.210.71.101/bityad')

ent_url_count = tk.Entry(master=frm_entry_1, width=8)
lbl_url_count = tk.Label(master=frm_entry_1, text="Enter Number of Parts: ")

ent_name.grid(row=0, column=1)
lbl_name.grid(row=0, column=0)

ent_url.grid(row=1, column=1)
lbl_url.grid(row=1, column=0)
ent_url_count.grid(row=2, column=1)
lbl_url_count.grid(row=2, column=0)

label_file_explorer = tk.Label(master=window,  
                            text = "Opened File will shows here!",  
                            fg = "blue") 

btn_convert_1 = tk.Button(
    master=window,
    text="Run Folder Name",
    command=name_to_folder_name
)
btn_convert_2 = tk.Button(
    master=window,
    text="Run Rar File Name",
    command=name_to_Rar_file_name
)

btn_url = tk.Button(
    master=window,
    text="Run Url Maker",
    command=full_url_maker
)

btn_open_file = tk.Button(
    master=window,
    text="Open Text (.txt) File",
    command=browseFiles
)


lbl_result_1_msg = tk.Label(master=window, text="...")

lbl_result_2_msg = tk.Label(master=window, text="...")

lbl_url_msg=tk.Label(master=window, text="...")


frm_entry_1.grid(row=1, column=0)
frm_entry_2.grid(row=4, column=0)

btn_convert_1.grid(row=3, column=0)
btn_convert_2.grid(row=4, column=0)
btn_url.grid(row=5, column=0)
btn_open_file.grid(row=6, column=0)

lbl_result_1_msg.grid(row=3, column=1)

lbl_result_2_msg.grid(row=4, column=1)

lbl_url_msg.grid(row=5, column=1)

label_file_explorer.grid(row=6, column=1)

btn_add_to_file = tk.Button(
    master=window,
    text="Add Urls to End of the File",
    command=add_to_end_of_the_file
    )

add_to_file_msg = tk.Label(master=window, text="...")
btn_add_to_file.grid(row=7, column=0)
add_to_file_msg.grid(row=7, column=1)

window.mainloop()
